# TP3 IHM Avancée - INFO5 Polytech

Etudiants:
- Arnoux Thibaut
- Lordey Maxime
- Rakotoarimalala Mandresy

## Fonctionnement

Il est possible d'effectuer la translation d'un élément, d'un point à un autre. On peut agrandir/réduire les éléments ainsi que les faire pivoter, il est aussi possible d'effectuer les deux actions en même temps.
Dans l'état rotozoom, si l'on relache un doigt, on peut continuer à effectuer une translation.

Cependant si l'on touche deux elements en même temps le comportement est aléatoire. Pour la vidéo on peut la translater sans qu'elle soit lancer, cependant, pour pouvoir effectuer un zoom ou une rotation ou les deux, il faut qu'elle soit lancer.


## Machine à état

Pour mieux comprendre l'application, nous avons posé la machine à états.

![](./machineEtats.png)
