# TP1 IHM Avancée - INFO5 Polytech

Etudiants:
- Arnoux Thibaut  
- Lordey Maxime  
- Rakotoarimalala Mandresy  

## Développement du Range Slider

### Conception

Pour réaliser ce Range Slider, nous avons implementé notre propre composant swing, en utilisant le model mvc (Model, View, Controller) suivant:  

![](./docs/MVC.png)  

- La vue s'abonne au JComponent ce qui permet de récupérer  les informations de l'utilisateur (MousePressed, MouseDragged, ...) et de les transmettre au controller. la vue s'abonne également au modèle pour être informé des modifications.  
- Le modèle possède les données du widget comme la valeur des curseurs, les bornes minimales et maximales du Range Slider.  
- Le controller quand à lui possède la machine à états qui permet de savoir quoi faire selon les informations transmises par la vue, et de prévenir le model pour qu'il modifie ces attributs.  

![](./docs/machineEtats.png)  

### Implémentation

Pour utiliser le Range Slider, il faut sélectionner l'un des 2 curseurs et le déplacer. Lorsque les 2 curseurs sont superposés, c'est toujours le même curseur qui est appelé car il est prioritaire.  
L'intervalle de sélection est celui qui est entre les 2 curseurs et mis en bleu clair.  
Le fait de clicker à un endroit sur le range slider où il n'y a pas de curseur ne déplace pas le curseur.  

## Home Finder

![](./docs/Homefinder.png)

Pour le home finder, chaque carré représente une maison. Chaque maison possède des coordonnées, un nombre de chambres compris entre 0 et 6 (inclus), ainsi qu'un prix.  
L'utilisateur peut influencer sur différents critères comme le nombre de chambres, le prix ainsi que la distance par rapport au centre.  
Interagir avec les différents range slider permet de rendre visible les maisons qui correspondent aux critères selectionnés.
