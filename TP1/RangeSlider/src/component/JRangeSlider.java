package component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import component.controller.JRangeSliderController;
import component.model.JRangeSliderModel;
import component.model.JRangeSliderModelListener;
import component.view.JRangeSliderView;

public class JRangeSlider extends JComponent{
	private static final String uiClassID = "JRangeSliderUI";
	
	private JRangeSliderController _controller;
	private JRangeSliderModel _model;
	
	
	public JRangeSlider() {
		_model = new JRangeSliderModel(0, 200, 20, 180);
		_controller = new JRangeSliderController(_model);
		updateUI();
	}
	
	public JRangeSlider(int valeurmin, int valeurmax, int cursor1, int cursor2) {
		// TODO Auto-generated constructor stub
		_model = new JRangeSliderModel(valeurmin,valeurmax,cursor1,cursor2);
		_controller = new JRangeSliderController(_model);
		updateUI();
	}

	@Override
	public void updateUI() {
		JRangeSliderView view = new JRangeSliderView(_controller, this);
		_model.addListener(view);
		super.setUI(view);		
	}
	
	public JRangeSliderView getUI() {
		return (JRangeSliderView) ui;
	}
	
	public JRangeSliderModel getModel() {
		return _model;
	}
	
	public void addListener(JRangeSliderModelListener l) {
		_model.addListener( l);
	}
	
	public void removeListener(JRangeSliderModelListener l) {
		_model.removeListener( l);
	}
	
}
