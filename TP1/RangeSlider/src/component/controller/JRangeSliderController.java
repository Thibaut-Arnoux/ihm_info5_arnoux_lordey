package component.controller;

import java.awt.Point;
import java.awt.event.MouseEvent;

import component.model.JRangeSliderModel;


public class JRangeSliderController {

	JRangeSliderModel _model;
	Element _component;
	Point _point;
	State _state;

	public enum Element {
		CURSOR_1,CURSOR_2,VOID
	}

	enum State {
		IDLE, WAITING, DRAGGING
	}

	public JRangeSliderController(JRangeSliderModel model) {
		_model = model;
		_component = Element.VOID;
		_state = State.IDLE;
		_point = null;

	}

	public void mouseDragged(MouseEvent e,Element component, float ratio) {
		// TODO Auto-generated method stub
		int new_value=0;
		switch(_state) {
		case WAITING:
			switch (_component) {
			case CURSOR_1:
				new_value = getNewValue(ratio);
				_model.setCursor1(new_value);
				break;
			case CURSOR_2:
				new_value = getNewValue(ratio);
				_model.setCursor2(new_value);								
				break;
			case VOID:
				throw new IllegalArgumentException();
			default:
				throw new IllegalArgumentException();								
			}
			_state = State.DRAGGING;
			break;
		case DRAGGING:
			switch (_component) {
			case CURSOR_1:
				new_value = getNewValue(ratio);
				_model.setCursor1(new_value);
				break;
			case CURSOR_2:
				new_value = getNewValue(ratio);
				_model.setCursor2(new_value);								
				break;
			case VOID:
				throw new IllegalArgumentException();
			default:
				throw new IllegalArgumentException();								
			}
			_state = State.DRAGGING;
			break;
		case IDLE:
			_state = State.IDLE;
			break;
		default:
			throw new IllegalArgumentException();					
		}
		_point = e.getPoint();

	}

	public void mousePressed(MouseEvent e,Element component) {
		// TODO Auto-generated method stub
		switch(component) {
		case CURSOR_1:
			_component = Element.CURSOR_1;
			_state = State.WAITING;
			break;
		case CURSOR_2:
			_component = Element.CURSOR_2;
			_state = State.WAITING;
			break;
		case VOID:
			_component = Element.VOID;
			_state = State.IDLE;
			break;
		default:
			throw new IllegalArgumentException();
		}
		_point = e.getPoint();
	}


	public void mouseReleased(MouseEvent e,Element component) {
		// TODO Auto-generated method stub
		_component = Element.VOID;
		_state = State.IDLE;			
	}

	public int getNewValue(float ratio) {
		int min = _model.getMin();
		int max = _model.getMax();

		return (int) (min + ratio * (max-min));

	}

}
