package component.model;


import javax.swing.event.EventListenerList;

public class JRangeSliderModel {
	
	protected int _min;
	protected int _max;
	protected int _cursor1;
	protected int _cursor2;
	protected EventListenerList _listenerList;
	
	public JRangeSliderModel() {
		_min = _max = _cursor1 = _cursor2 = 0;
		_listenerList = new EventListenerList();
	}
	
	public JRangeSliderModel(int min, int max) {
		// TODO Verification min < max
		_min = min;
		_max = max;
		_cursor1 = _min;
		_cursor2 = _max;
		_listenerList = new EventListenerList();
	}
	
	public JRangeSliderModel(int min, int max, int cursor1, int cursor2) { 
		// TODO Verification min < max
		_min = min;
		_max = max;
		_listenerList = new EventListenerList();
		this.setCursor1(cursor1);
		this.setCursor2(cursor2);
	}
	
	
	public void setMax(int max) {
		int oldValue = _max;
			
		if(max >= _cursor1 && max >= _cursor2 && max >= _min) {
			_max = max;
		} else {
			_max = Math.max(_min, Math.max(_cursor1, _cursor2));
		}
		
		for(JRangeSliderModelListener listener : _listenerList.getListeners(JRangeSliderModelListener.class)) {
			listener.onCursor1Change(oldValue, _max);
		}
	}	
	
	public void setMin(int min) {
		int oldValue = _min;
		if(min <= _cursor1 && min <= _cursor2 && min <= _max) {
			_min = min;
		} else {
			_min = Math.min(_max, Math.min(_cursor1, _cursor2));
		}
		
		for(JRangeSliderModelListener listener : _listenerList.getListeners(JRangeSliderModelListener.class)) {
			listener.onCursor1Change(oldValue, _min);
		}
	}	
	
	public void setCursor1(int cursor1) {
		int oldValue = _cursor1;
		
		_cursor1 = Math.min(_max, Math.max(_min, cursor1));
		
		for(JRangeSliderModelListener listener : _listenerList.getListeners(JRangeSliderModelListener.class)) {
			listener.onCursor1Change(oldValue, _cursor1);
		}
		
	}	
	
	public void setCursor2(int cursor2) {
		int oldValue = _cursor2;

		_cursor2 = Math.min(_max, Math.max(_min, cursor2));
		
		for(JRangeSliderModelListener listener : _listenerList.getListeners(JRangeSliderModelListener.class)) {
			listener.onCursor2Change(oldValue, _cursor2);
		}
	}
	
	
	public int getMax() {
		return _max;
	}
	
	public int getMin() {
		return _min;
	}
	
	public float getCursor1() {
		return _cursor1;
	}
	
	public float getCursor2() {
		return _cursor2;
	}
	
	public void addListener(JRangeSliderModelListener l) {
		_listenerList.add(JRangeSliderModelListener.class, l);
		
		// Envoie des données lors de l'abonnement
		l.onMaxChange(_max, _max);
		l.onMinChange(_min, _min);
		l.onCursor1Change(_cursor1, _cursor1);
		l.onCursor2Change(_cursor2, _cursor2);
	}
	
	public void removeListener(JRangeSliderModelListener l) {
		_listenerList.remove(JRangeSliderModelListener.class, l);
	}
}
