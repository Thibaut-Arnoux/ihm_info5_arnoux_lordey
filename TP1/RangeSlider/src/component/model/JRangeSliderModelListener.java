package component.model;

import java.util.EventListener;

public interface JRangeSliderModelListener extends EventListener {

	public void onMinChange(int oldValue, int newValue);
	
	public void onMaxChange(int oldValue, int newValue);
	
	public void onCursor1Change(int oldValue, int newValue);
	
	public void onCursor2Change(int oldValue, int newValue);
}
