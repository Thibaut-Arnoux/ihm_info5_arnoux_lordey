package component.view;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

import component.controller.JRangeSliderController;
import component.controller.JRangeSliderController.Element;
import component.model.JRangeSliderModelListener;

public class JRangeSliderView extends ComponentUI implements JRangeSliderModelListener, ComponentListener{
	public final static int LINEHEIGTH = 5;
	public final static int HIGHLIGHTHEIGTH = 7;
	public final static int CURSORSIZE = 17;
	public final static int PADDING = 21;

	public final static Color CURSORCOLOR = new Color(204, 204, 204);
	public final static Color LINECOLOR = new Color(128, 128, 128);
	public final static Color HIGHLIGHTCOLOR = new Color(0, 153, 204);

	private JRangeSliderController _controller;
	private JComponent _component;

	// Graphical Component
	private Ellipse2D.Float _cursor1Ellipse;
	private Ellipse2D.Float _cursor2Ellipse;
	private RoundRectangle2D _line;
	private RoundRectangle2D _highlight;



	//Model 
	private int _min;
	private int _max;
	private int _cursor1;
	private int _cursor2;


	public JRangeSliderView(JRangeSliderController c, JComponent component) {
		_controller = c;
		_component = component;
		_component.addMouseListener(new MouseHandler());
		_component.addMouseMotionListener(new MouseHandler());
		_component.addComponentListener(this);

		_cursor1Ellipse = new Ellipse2D.Float(PADDING, getCenterHeight()-(CURSORSIZE/2), CURSORSIZE, CURSORSIZE);
		_cursor2Ellipse = new Ellipse2D.Float(PADDING, getCenterHeight()-(CURSORSIZE/2), CURSORSIZE, CURSORSIZE);
		_line = new RoundRectangle2D.Float(PADDING,getCenterHeight()-(LINEHEIGTH/2),getLineWidth(),LINEHEIGTH,LINEHEIGTH,LINEHEIGTH);
		_highlight = new RoundRectangle2D.Float(PADDING,getCenterHeight()-(HIGHLIGHTHEIGTH/2),getLineWidth(),HIGHLIGHTHEIGTH,HIGHLIGHTHEIGTH,HIGHLIGHTHEIGTH);

		_min = _max = 0;		
	}


	@Override
	public void onMinChange(int oldValue, int newValue) {
		_min = newValue;
	}


	@Override
	public void onMaxChange(int oldValue, int newValue) {
		_max = newValue;
	}

	@Override
	public void onCursor1Change(int oldValue, int newValue) {
		_cursor1 = newValue;		
		int location = getLocationY(newValue,_min,_max);		
		_cursor1Ellipse.setFrame(location-_cursor1Ellipse.getWidth()/2, getCenterHeight()-(CURSORSIZE/2), _cursor1Ellipse.width, _cursor1Ellipse.height);
		updateHighlight();
		_component.repaint();

	}


	@Override
	public void onCursor2Change(int oldValue, int newValue) {
		_cursor2 = newValue;
		int location = getLocationY(newValue,_min,_max);
		_cursor2Ellipse.setFrame(location-_cursor2Ellipse.getWidth()/2, getCenterHeight()-(CURSORSIZE/2), _cursor2Ellipse.width, _cursor2Ellipse.height);
		updateHighlight();
		_component.repaint();
	}

	private int getLocationY(int y,int min, int max) {
		int linewidth = getLineWidth();
		float ratio = (y - min) / (float)(max - min) ;
		int location = (int) (linewidth * ratio);	
		return location + PADDING;
	}

	public int getCenterHeight() {
		return _component.getHeight()/2;
	}

	public int getLineWidth() {
		return _component.getWidth() - (PADDING*2);
	}

	public void updateHighlight() {
		int debut = (int) Math.min(_cursor1Ellipse.x+_cursor1Ellipse.getWidth()/2,_cursor2Ellipse.x+_cursor2Ellipse.getWidth()/2);
		int fin = (int) (Math.max(_cursor1Ellipse.x+_cursor1Ellipse.getWidth()/2,_cursor2Ellipse.x+_cursor2Ellipse.getWidth()/2) - debut);

		_highlight.setRoundRect(debut,getCenterHeight()-(HIGHLIGHTHEIGTH/2),fin,HIGHLIGHTHEIGTH,HIGHLIGHTHEIGTH,HIGHLIGHTHEIGTH);
	}

	public void updateLine() {
		_line.setRoundRect(PADDING,getCenterHeight()-(LINEHEIGTH/2),getLineWidth(),LINEHEIGTH,LINEHEIGTH,LINEHEIGTH);
	}


	@Override
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
		System.out.println("resize");

		int location_cursor1 = getLocationY(_cursor1,_min,_max);
		_cursor1Ellipse.setFrame(location_cursor1-_cursor1Ellipse.getWidth()/2, getCenterHeight()-(CURSORSIZE/2), _cursor1Ellipse.width, _cursor1Ellipse.height);

		int location_cursor2 = getLocationY(_cursor2,_min,_max);		
		_cursor2Ellipse.setFrame(location_cursor2-_cursor2Ellipse.getWidth()/2, getCenterHeight()-(CURSORSIZE/2), _cursor2Ellipse.width, _cursor2Ellipse.height);

		updateHighlight();
		updateLine();

		_component.repaint();
	}


	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub	
	}


	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void paint(Graphics g, JComponent c) {
		super.paint(g, c);
		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(LINECOLOR);
		g2.fill(_line);

		g2.setColor(HIGHLIGHTCOLOR);
		g2.fill(_highlight);

		g2.setColor(CURSORCOLOR);
		g2.fill(_cursor1Ellipse);
		g2.setColor(CURSORCOLOR);
		g2.fill(_cursor2Ellipse);
		
		g2.setColor(HIGHLIGHTCOLOR);
		g2.drawString(""+_cursor1, _cursor1Ellipse.x, _cursor1Ellipse.y);
		g2.drawString(""+_cursor2, _cursor2Ellipse.x, _cursor2Ellipse.y);
	}



	public class MouseHandler implements MouseListener, MouseMotionListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			Point point = e.getPoint();
			Element component;
			if (_cursor1Ellipse.contains(point)) {
				component = Element.CURSOR_1;
			} else if(_cursor2Ellipse.contains(point)) {
				component = Element.CURSOR_2;				
			} else {
				component = Element.VOID;				
			}

			_controller.mousePressed(e,component);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			Point point = e.getPoint();
			Element component;
			if (_cursor1Ellipse.contains(point)) {
				component = Element.CURSOR_1;
			} else if(_cursor2Ellipse.contains(point)) {
				component = Element.CURSOR_2;				
			} else {
				component = Element.VOID;				
			}

			_controller.mouseReleased(e,component);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			Point point = e.getPoint();
			Element component;
			if (_cursor1Ellipse.contains(point)) {
				component = Element.CURSOR_1;
			} else if(_cursor2Ellipse.contains(point)) {
				component = Element.CURSOR_2;				
			} else {
				component = Element.VOID;				
			}

			float ratio = (e.getX()-PADDING) / ((float) getLineWidth());
			_controller.mouseDragged(e,component, ratio);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}


}
