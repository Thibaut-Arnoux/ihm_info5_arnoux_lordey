package home;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import main.Main;

public class Carte extends Canvas{
	private final static int NBHOMES = 100; 
	private final static ArrayList<Home> HOMES = new ArrayList<Home>(NBHOMES);
	private final static int RECTSIZE = 9;
	
	
	protected int _valeurMin;
	protected int _valeurMax;
	
	protected int _bedroomMin;
	protected int _bedroomMax;
	
	protected double _distanceMin;
	protected double _distanceMax;
	
	public Carte() {
		_valeurMin = Home.VALEURMIN;
		_valeurMax = Home.VALEURMAX;
		_bedroomMin = Home.PIECESMIN;
		_bedroomMax = Home.PIECESMAX;
		_distanceMin = 0;
		_distanceMax = 900;
		populate();
	}
	
	public void paint(Graphics g) {
		Point2D.Float localisation;
		int pieces;
		int valeur;
		double distance;
		boolean displayable;
		
		for (Home home : HOMES) {
			localisation = home.get_position();
			pieces = home.get_bedroom();
			valeur = home.get_valeur();
			distance = localisation.distance(new Point2D.Float(Main.WIDTH/2,Main.HEIGHT/2));
			
			displayable = (pieces >= _bedroomMin && pieces <= _bedroomMax) && 
					(valeur >= _valeurMin && valeur <= _valeurMax) &&
					(distance >= _distanceMin && distance <= _distanceMax);
		
			
			if (displayable) {
				g.setColor(Color.RED);
				g.fillRect((int)( localisation.x+(RECTSIZE/2)), (int)(localisation.y+(RECTSIZE/2)), RECTSIZE, RECTSIZE);
			}
		}
	}
	
	public static void populate() {
		HOMES.clear();
		int pieces;
		int valeur;
		float x;
		float y;
		for (int i = 0 ; i < NBHOMES ; i++) {
			pieces = (int) (Home.PIECESMIN + Math.random() * (Home.PIECESMAX - Home.PIECESMIN +1));
			valeur = (int) (Home.VALEURMIN + Math.random() * (Home.VALEURMAX - Home.VALEURMIN +1));
			x = (float) (Math.random() * (Main.WIDTH-30));
			y = (float) (Math.random() * (Main.HEIGHT-30));
			
			HOMES.add(new Home(new Point2D.Float(x, y),pieces,valeur));
		}
	}

	public void set_valeurMin(int _valeurMin) {
		this._valeurMin = _valeurMin;
	}

	public void set_valeurMax(int _valeurMax) {
		this._valeurMax = _valeurMax;
	}

	public void set_bedroomMin(int _piecesMin) {
		this._bedroomMin = _piecesMin;
	}

	public void set_bedroomMax(int _piecesMax) {
		this._bedroomMax = _piecesMax;
	}

	public void set_distanceMin(double _distanceMin) {
		this._distanceMin = _distanceMin;
	}

	public void set_distanceMax(double _distanceMax) {
		this._distanceMax = _distanceMax;
	}
}
