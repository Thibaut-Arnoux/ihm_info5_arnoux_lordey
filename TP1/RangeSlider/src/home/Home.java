package home;

import java.awt.geom.Point2D;

public class Home {
	public static final int  PIECESMIN=0;
	public static final int  PIECESMAX=6;
	public static final int  VALEURMIN=0;
	public static final int  VALEURMAX=100000;
	
	
	protected Point2D.Float _position;
	protected int _bedroom;
	protected int _valeur;
	
	public Home(Point2D.Float position,int pieces,int valeur) {
		_position = position;
		_bedroom = pieces;
		_valeur = valeur;
	}

	public Point2D.Float get_position() {
		return _position;
	}

	public int get_bedroom() {
		return _bedroom;
	}

	public int get_valeur() {
		return _valeur;
	}
	
	
	
}
