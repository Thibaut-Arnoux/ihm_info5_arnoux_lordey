package main;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import component.JRangeSlider;
import component.model.JRangeSliderModelListener;
import home.Carte;
import home.Home;

public class Main {
	public static final int WIDTH=800;
	public static final int HEIGHT=600;

	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		JFrame frame = new JFrame();
		
		JPanel panelCentrale = new JPanel();
		panelCentrale.setLayout(new BorderLayout());
		
		JPanel panelLateral = new JPanel();
		panelLateral.setLayout(new GridLayout(3,1));
		
		Carte carte = new Carte();
		
		JPanel valeurPanel = new JPanel(new GridLayout(2,1));
		JComponent valeurName = new JLabel("Cost");
		JRangeSlider valeur = new JRangeSlider(Home.VALEURMIN,Home.VALEURMAX,Home.VALEURMIN,Home.VALEURMAX);
		valeur.addListener(new ValeurHandler(carte));
		valeurPanel.add(valeurName);
		valeurPanel.add(valeur);

		JPanel distancePanel = new JPanel(new GridLayout(2,1));
		JComponent distanceName = new JLabel("Distance to Center");
		JRangeSlider distance = new JRangeSlider(0,WIDTH+100,0,WIDTH+100);
		distance.addListener(new DistanceHandler(carte));
		distancePanel.add(distanceName);
		distancePanel.add(distance);

		JPanel bedroomPanel = new JPanel(new GridLayout(2,1));
		JComponent bedroomName = new JLabel("BedRooms");
		JRangeSlider bedroom = new JRangeSlider(Home.PIECESMIN,Home.PIECESMAX,Home.PIECESMIN,Home.PIECESMAX);
		bedroom.addListener(new BedroomHandler(carte));
		bedroomPanel.add(bedroomName);
		bedroomPanel.add(bedroom);
		
		panelLateral.add(bedroomPanel);
		panelLateral.add(valeurPanel);
		panelLateral.add(distancePanel);
		
		panelCentrale.add(carte,BorderLayout.CENTER);
		panelCentrale.add(panelLateral,BorderLayout.EAST);
		frame.setContentPane(panelCentrale);			
		
	    //Nous demandons maintenant à notre objet de se positionner au centre
	    frame.setLocationRelativeTo(null);
	    //Termine le processus lorsqu'on clique sur la croix rouge
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //frame.setResizable(false);
	    frame.setSize(new Dimension(WIDTH,HEIGHT));
		frame.setVisible(true);
	}
	
	static class ValeurHandler implements JRangeSliderModelListener {
		
		protected int _cursor1=0;
		protected int _cursor2=0;
		protected Carte _carte;
		
		public ValeurHandler(Carte carte) {
			_carte = carte;
		}

		@Override
		public void onMinChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onMaxChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCursor1Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor1 = newValue;
			_carte.set_valeurMax(Math.max(_cursor1, _cursor2));
			_carte.set_valeurMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}

		@Override
		public void onCursor2Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor2 = newValue;			
			_carte.set_valeurMax(Math.max(_cursor1, _cursor2));
			_carte.set_valeurMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}
		
	}

	static class BedroomHandler implements JRangeSliderModelListener {
		
		protected int _cursor1=0;
		protected int _cursor2=0;
		protected Carte _carte;
		
		public BedroomHandler(Carte carte) {
			_carte = carte;
		}

		@Override
		public void onMinChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onMaxChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCursor1Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor1 = newValue;
			_carte.set_bedroomMax(Math.max(_cursor1, _cursor2));
			_carte.set_bedroomMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}

		@Override
		public void onCursor2Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor2 = newValue;			
			_carte.set_bedroomMax(Math.max(_cursor1, _cursor2));
			_carte.set_bedroomMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}
		
	}
	
	static class DistanceHandler implements JRangeSliderModelListener {

		protected int _cursor1=0;
		protected int _cursor2=0;
		protected Carte _carte;
		
		public DistanceHandler(Carte carte) {
			_carte = carte;
		}
		
		@Override
		public void onMinChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onMaxChange(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCursor1Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor1 = newValue;
			_carte.set_distanceMax(Math.max(_cursor1, _cursor2));
			_carte.set_distanceMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}

		@Override
		public void onCursor2Change(int oldValue, int newValue) {
			// TODO Auto-generated method stub
			_cursor2 = newValue;			
			_carte.set_distanceMax(Math.max(_cursor1, _cursor2));
			_carte.set_distanceMin(Math.min(_cursor1, _cursor2));
			_carte.repaint();
		}
		
	}
}
