package markedMenu.model.composite;

import static org.junit.Assert.*;

import java.util.Stack;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ComponentTest {
	Composite root;

	@Before
	public void setUp() {
		root = new Composite("Menu");
	}

	@After
	public void tearDown() {
		root.clear();
	}

	@Test
	public void testAddingComponent() {	
		int elements = 10;
		for (int i=0;i<elements;i++) {
			root.addComponent(new Leaf(""+i));
		}
		
		assertEquals(root.getChildsCount(),elements);
		
		root.clear();
	}

	@Test
	public void testRemovingComponent() {
		Stack<Component> stack = new Stack<Component>();
		int elements = 10;
		for (int i=0;i<elements;i++) {
			Component c = new Leaf(""+i);
			root.addComponent(c);
			stack.add(c);
		}
		
		for (int i=0;i<elements;i++) {
			Component c = stack.pop();
			root.removeComponent(c);
		}
		
		
		
		assertEquals(root.getChildsCount(),0);
		
	}
}
