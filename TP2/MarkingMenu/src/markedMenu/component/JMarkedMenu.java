package markedMenu.component;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;

import javax.swing.JComponent;

import markedMenu.model.DefaultMarkedMenuModel;
import markedMenu.model.composite.Component;
import markedMenu.uiDelegate.MarkedMenuIU;

public class JMarkedMenu extends JComponent implements ActionListener{
	
	private static final String uiClassID = "MarkedMenuUI";
	
	private DefaultMarkedMenuModel _model;
		
	public JMarkedMenu() {
		this.setVisible(false);
		_model = new DefaultMarkedMenuModel();
		addActionListener(this);
		updateUI();
	}

	public DefaultMarkedMenuModel getModel() {
        return _model;
    }
	
	@Override
	public void updateUI() {
		MarkedMenuIU view = new MarkedMenuIU(this);
		_model.addChangeListener(view);
		super.setUI(view);		
	}
	
	public void addComponent(Component c) {
		_model.addComponent(c);
	}
	
	public void removeComponent(Component c) {
		_model.removeComponent(c);
	}
	
	@Override
    public Dimension getPreferredSize()
    {
        return new Dimension(200, 200);
    }
	
	public Point getCenter() {
		return new Point(this.getLocation().x + this.getWidth()/2, this.getLocation().y + this.getHeight()/2);
	}
	
	public void setCenter(Point p) {
		this.setLocation(p.x - this.getWidth()/2, p.y - this.getHeight()/2);
	}
	
	public void activated(Point p) {		
		this.setCenter(p);
		_model.setDefault();
		((MarkedMenuIU)ui).setDefaultState();
		this.setVisible(true);
	}
	
	public void desactivated() {
		this.setVisible(false);
		_model.setDefault();
		((MarkedMenuIU)ui).setDefaultState();
	}

	public void setExpertMode(boolean b) {
		// TODO Auto-generated method stub
		_model.setExpertMode(b);
	}

	public void addActionListener(ActionListener l) {
		_model.addActionListener(l);		
	}
	
	public void removeActionListener(ActionListener l) {
		_model.removeActionListener(l);		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		desactivated();	
		
	}
	
	@Override
	public void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if(e.getButton() == MouseEvent.BUTTON3)
			desactivated();
		
	}

	public boolean getExpertMode() {
		// TODO Auto-generated method stub
		return _model.getExpertMode();
	}
	
}
