package markedMenu.component;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.hamcrest.CoreMatchers;

import markedMenu.model.composite.Component;
import markedMenu.model.composite.Composite;
import markedMenu.model.composite.Leaf;

public class mainMarkedMenu {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame();
		
		JMarkedMenu markedMenu = new JMarkedMenu();
		// Create component of markedmenu
		Component ellipse = new Leaf("Ellipse");
		Component rectangle = new Leaf("Rectangle");
		Component blue = new Leaf("Blue");
		Component red = new Leaf("Red");
		Composite colors = new Composite("colors");
		colors.addComponent(blue);
		colors.addComponent(red);
		
		markedMenu.addComponent(ellipse);
		markedMenu.addComponent(rectangle);
		markedMenu.addComponent(colors);
		markedMenu.setLocation(frame.getWidth()/2, frame.getHeight()/2);
		markedMenu.setSize(markedMenu.getPreferredSize());
		markedMenu.setExpertMode(false);
		
		
		
		frame.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) {
					markedMenu.desactivated();
				}
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {

				
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) {
					markedMenu.activated(e.getPoint());
				}
			}
		});

		
	    frame.setSize(new Dimension(500,500));
		frame.setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.add(markedMenu);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //frame.setResizable(false);
		frame.setVisible(true);
	}
	
}