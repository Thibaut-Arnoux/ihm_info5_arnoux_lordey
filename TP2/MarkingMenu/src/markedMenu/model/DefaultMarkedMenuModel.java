package markedMenu.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import markedMenu.model.composite.Component;
import markedMenu.model.composite.Composite;

public class DefaultMarkedMenuModel implements ActionListener, ChangeListener, ItemListener{

	// This is the root Menu.
	protected Composite _root;

	// The current Menu used
	protected Composite _currentComponent;

	protected EventListenerList _listenerList;

	protected ChangeEvent changeEvent = new ChangeEvent(this);

	// Indicate if the expertMode is enable.
	protected boolean _expertMode;

	public DefaultMarkedMenuModel() {
		_listenerList = new EventListenerList();
		_root = new Composite("Root");
		_currentComponent = _root;
		_expertMode = false;

		fireChangeEvent();
	}

	public void setExpertMode(boolean b) {
		if (b == _expertMode)
			return;

		_expertMode = b;

		fireChangeEvent();
	}

	public boolean getExpertMode() {
		return _expertMode;
	}

	public void setDefault() {
		unsubcribeComponent(_currentComponent);
		subcribeComponent(_root);
		_currentComponent = _root;

		fireChangeEvent();
	}

	public Composite getCurrentComponent() {
		return _currentComponent;
	}

	public void addComponent(Component c) {
		_root.addComponent(c);

		if(_currentComponent == _root) {
			c.addActionListener(this);
			fireChangeEvent();
		}
	}

	public void removeComponent(Component c) {
		_root.removeComponent(c);

		throw new UnsupportedOperationException("TO HARD FOR US.");
	}

	public void unsubcribeComponent(Component c) {
		if(!c.isLeaf()) {
			Component[] components = ((Composite)c).getChilds();

			for(int i=0; i < components.length;i++) {
				components[i].removeActionListener(this);
				components[i].removeChangeListener(this);
			}

		}
	}

	public void subcribeComponent(Component c) {		
		if(!c.isLeaf()) {			
			Component[] components = ((Composite)c).getChilds();

			for(int i=0; i < components.length;i++) {
				components[i].addActionListener(this);
				components[i].addChangeListener(this);
			}				
		}
	}

	public void addChangeListener(ChangeListener l) {
		_listenerList.add(ChangeListener.class, l);		
	}

	public void removeChangeListener(ChangeListener l) {
		_listenerList.remove(ChangeListener.class, l);		
	}

	public ChangeListener[] getChangeListeners() {
		// return all registered ChangeListener objects
		return (ChangeListener[]) _listenerList.getListeners(ChangeListener.class);
	}

	public void fireChangeEvent() {
		ChangeListener[] changeListener = getChangeListeners();

		for(int i=0; i<changeListener.length; i++) {
			changeListener[i].stateChanged(changeEvent);
		}
	}

	@Override
	public void itemStateChanged(ItemEvent l) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stateChanged(ChangeEvent l) {
		// TODO : Notify the UI

		fireChangeEvent();
	}


	@Override
	public void actionPerformed(ActionEvent l) {
		Component c = (Component)l.getSource();


		if(c.isLeaf()) {
			// TODO : Item Selected and performed action in Paint + Delete MarkedMenu in UI
			setDefault();
			fireActionPerformed(new ActionEvent(c, ActionEvent.ACTION_PERFORMED, new String())); 
		} else {
			// TODO : Notify Ui paint the new Composite selected
			Composite node = (Composite)c;

			if (node.getChildsCount() != 0) {
				unsubcribeComponent(_currentComponent);
				subcribeComponent(node);
				_currentComponent = node;
				fireChangeEvent();
			}
		}
	}

	public void addActionListener(ActionListener l) {
		_listenerList.add(ActionListener.class, l);		
	}

	public void removeActionListener(ActionListener l) {
		_listenerList.remove(ActionListener.class, l);		
	}


	public ActionListener[] getActionListeners() {
		return (ActionListener[]) _listenerList.getListeners(ActionListener.class);
	}

	protected void fireActionPerformed(ActionEvent e) {
		ActionListener[] ll = getActionListeners();

		for (int i = 0; i < ll.length; i++)
			ll[i].actionPerformed(e);
	}
}
