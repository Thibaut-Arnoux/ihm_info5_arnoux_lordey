package markedMenu.model.composite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public abstract class Component implements ButtonModel{
	
	//Indicates partial commitment towards triggering the button.
	public final static int ARMED=1;	
	
	//Indicates if the button can be selected or triggered by an input device, such as a mouse pointer.
	public final static int ENABLED=2;	
	
	//Indicates if the button is pressed.
	public final static int PRESSED=4;	
	
	//Indicates that the mouse is over the button.
	public final static int ROLLOVER=8;
	
	//Indicates if the button has been selected.
	public final static int SELECTED=16;
	
	//Indicates the name of the Component that will be displayed in the look-and-feel objects.
	protected String _name;
	
	//Indicates the current state(Armed,Enable,Pressed,...) of the Component.
	protected int _state;
	
	
	protected EventListenerList listenerList = new EventListenerList();
	
	protected ChangeEvent changeEvent = new ChangeEvent(this);

	public Component(String name) {
		_name = name;
		_state = ENABLED;
	}
	
	/**
	 * 
	 * @return if the Component is a Leaf or a Composite
	 */
	public abstract boolean isLeaf();
	
	@Override
	public String toString() {
		return _name;
	}
	
	@Override
	public boolean isArmed() {
		// TODO Auto-generated method stub
		return (_state & ARMED) == ARMED;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return (_state & ENABLED) == ENABLED;
	}

	@Override
	public boolean isPressed() {
		// TODO Auto-generated method stub
		return (_state & PRESSED) == PRESSED;
	}

	@Override
	public boolean isRollover() {
		// TODO Auto-generated method stub
		return (_state & ROLLOVER) == ROLLOVER;
	}

	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return (_state & SELECTED) == SELECTED;
	}

	@Override
	public void setArmed(boolean a) {
		// TODO Auto-generated method stub
		
		// If the Component is not enabled then it cannot be armed.
		if (!isEnabled())
			return;

		// If the function doesn't change the state of the Component.
		if ( (a && isArmed()) || (!a && !isArmed()))
			return;
		
		if(a) {
			_state = _state | ARMED;
			System.out.println(_name);
		} else {
			_state = _state & (~ARMED);			
		}
		
		// notify ChangeListeners
		fireStateChanged();
	}

	@Override
	public void setEnabled(boolean b) {
		// TODO Auto-generated method stub

		if ( (b && isEnabled()) || (!b && !isEnabled()))
			return;
		
		if(b) {
			_state = _state | ARMED;
		} else {
			_state = _state & (~ENABLED) & (~ARMED) & (~PRESSED);			
		}
		
		// notify ChangeListeners
		fireStateChanged();
		
	}
	
	@Override
	public void setPressed(boolean b) {
		// TODO Auto-generated method stub
		
		// If the Component is not enabled then it cannot be armed.
		if (!isEnabled())
			return;

		// If the function doesn't change the state of the Component.
		if ( (b && isPressed()) || (!b && !isPressed()))
			return;
		
		if(b) {
			_state = _state | PRESSED;
		} else {
			_state = _state & (~PRESSED);			
		}
		
		if(b)
			fireActionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, new String()));

	}

	@Override
	public void setRollover(boolean b) {
		// TODO Auto-generated method stub

		// If the function doesn't change the state of the Component.
		if ( (b && isRollover()) || (!b && !isRollover()))
			return;
		
		if(b) {
			_state = _state | ROLLOVER;
		} else {
			_state = _state & (~ROLLOVER);			
		}

		// notify ChangeListeners
		fireStateChanged();
	}

	@Override
	public void setSelected(boolean b) {
		// TODO Auto-generated method stub
		
		// If the Component is not enabled then it cannot be armed.
		if (!isEnabled())
			return;

		// If the function doesn't change the state of the Component.
		if ( (b && isSelected()) || (!b && !isSelected()))
			return;
		
		if(b) {
			_state = _state | SELECTED;
		} else {
			_state = _state & (~SELECTED);			
		}
		

		// notify ChangeListeners
		fireStateChanged();
		
		// fire ItemStateChanged events
		if (b) {
			fireItemStateChanged(new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED,
		                                           this, ItemEvent.SELECTED));
		} else {
			fireItemStateChanged(new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED,
		                                         this, ItemEvent.DESELECTED));		
		}
	}

	@Override
	public void addActionListener(ActionListener l) {
		listenerList.add(ActionListener.class, l);
	}

	@Override
	public void addChangeListener(ChangeListener l) {
		listenerList.add(ChangeListener.class, l);		
	}

	@Override
	public void addItemListener(ItemListener l) {
		listenerList.add(ItemListener.class, l);		
	}
		
	@Override
	public void removeActionListener(ActionListener l) {
		listenerList.remove(ActionListener.class, l);
	}

	@Override
	public void removeChangeListener(ChangeListener l) {
		listenerList.remove(ChangeListener.class, l);		
	}

	@Override
	public void removeItemListener(ItemListener l) {
		listenerList.remove(ItemListener.class, l);	
		
	}
	
	public ChangeListener[] getChangeListeners() {
		// return all registered ChangeListener objects
		return (ChangeListener[]) listenerList.getListeners(ChangeListener.class);
	}	
	
	public ActionListener[] getActionListeners() {
		// return all registered ActionListener objects
		return (ActionListener[]) listenerList.getListeners(ActionListener.class);
	}
	
	public ItemListener[] getItemListeners() {
		// return all registered ItemListener objects
		return (ItemListener[]) listenerList.getListeners(ItemListener.class);
	}
	
	protected void fireStateChanged() {
		ChangeListener[] changeListener = getChangeListeners();
		
		for(int i=0; i<changeListener.length; i++) 
			changeListener[i].stateChanged(changeEvent);
			
	}
	
	protected void fireActionPerformed(ActionEvent e) {
		ActionListener[] ll = getActionListeners();
		
		for (int i = 0; i < ll.length; i++)
			ll[i].actionPerformed(e);
	}
	
	protected void fireItemStateChanged(ItemEvent e) {
		ItemListener[] ll = getItemListeners();
		for (int i = 0; i < ll.length; i++)
			ll[i].itemStateChanged(e);
	}

	@Override
	public void setGroup(ButtonGroup arg0) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public void setMnemonic(int arg0) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public void setActionCommand(String arg0) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public String getActionCommand() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMnemonic() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] getSelectedObjects() {
		return null;
	}


}
