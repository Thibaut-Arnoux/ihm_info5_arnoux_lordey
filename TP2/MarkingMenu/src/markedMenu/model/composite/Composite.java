package markedMenu.model.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Composite extends Component{
	public static final int MAXCOMPONENTS = 8;

	private static final int LIST_CAPACITY = 7;

	// Contains the list of subCompononent of this composite.
	protected List<Component> _components;

	// Represent the 8th subComponent 
	protected Component _otherComponent;

	// Represent the 8th subComponent when there is more than 8 subcomponents
	protected Composite _otherComposite;

	public Composite(String name) {
		// TODO Auto-generated constructor stub
		super(name);
		_components = new Vector<Component>(LIST_CAPACITY);
		_otherComponent = null;
		_otherComposite = null;
	}

	public void addComponent(Component c) {
		
		if (_components.size() < LIST_CAPACITY) {
			_components.add(c);
		}else if (_components.size() == LIST_CAPACITY && _otherComponent == null){
			_otherComponent = c;
		}else if (_components.size() == LIST_CAPACITY && _otherComponent != _otherComposite)  {

			if (_otherComposite == null)
				_otherComposite = new Composite("Other");

			Component component = _otherComponent;
			_otherComposite.addComponent(component);
			_otherComposite.addComponent(c);
			_otherComponent = _otherComposite;
		}else {
			_otherComposite.addComponent(c);
		}
	}

	public boolean removeComponent(Component c) {
		if (_components.contains(c)) {
			_components.remove(c);
			return true;
		}else if (_otherComponent == c){
			_otherComponent = null;
			return true;
		}else if (_otherComponent != null && _otherComponent == _otherComposite){
			Composite node = _otherComposite;
			boolean success = node.removeComponent(c);
			int size = node.getChildsCount();
			if (success && size == 1) {
				Component child = node._components.get(0);
				_otherComponent = child;
			}			
			return success;
		} else {
			return false;
		}
	}

	public int getChildsCount() {
		int childsCount = _components.size();
		if (_otherComponent == null) {
			return childsCount;
		} else if(_otherComponent == _otherComposite) {
			return childsCount+_otherComposite.getChildsCount();
		} else {
			return childsCount+1;
		}
	}

	public Component[] getChilds() {

		if(_otherComponent != null) {
			Component[] components = new Component[MAXCOMPONENTS];			

			int i=0;
			for(Component c : _components) {
				components[i] = c;
				i++;
			}
			components[i] = _otherComponent;			
			return components;
		} else {
			return (Component[]) _components.toArray(new Component[getChildsCount()]);
		}

	}

	public Component getComponentByIndex(int i) {
		if(i > MAXCOMPONENTS || i > _components.size()) {
			return null; 
		} else {
			return i == LIST_CAPACITY ? _otherComponent : _components.get(i);
		}
	}

	@Override
	public boolean isLeaf() {
		// TODO Auto-generated method stub
		return false;
	}

	public void clear() {
		// TODO Auto-generated method stub
		_components.clear();
		if (_otherComponent != null) {
			if (_otherComponent == _otherComposite)
				_otherComposite.clear();

			_otherComponent = null;			
		}
	}
}
