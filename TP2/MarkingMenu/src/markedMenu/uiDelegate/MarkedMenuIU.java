package markedMenu.uiDelegate;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;

import markedMenu.component.JMarkedMenu;
import markedMenu.model.DefaultMarkedMenuModel;
import markedMenu.model.composite.Component;
import markedMenu.model.composite.Composite;

public class MarkedMenuIU extends ComponentUI implements MouseMotionListener, ComponentListener, ChangeListener, MouseListener{

	
	public final static float FULL_ROTATION = 360.0f;

	private final static int MAXCOMPONENTS = 8; 

	// Color when a Component is not Hovered
	private final static Color REST = new Color(191, 191, 191);
	
	// Color when a Component is Hovered
	private final static Color ARMED = new Color(230, 230, 230);


	private enum States{
		IDLE, INSIDE;
	}

	// The Component
	private JMarkedMenu _component;

	//
	// Variables used to paint the Component
	//
	// The outer circle inscribed in the Component.
	private Ellipse2D _outerCercle;

	// The arcs of circles that represent the sub-menu
	private CustomArc2D[] _arcs;

	private int nbElements;
	//
	// Variables used to paint the Component
	//
	
	//
	// Variables used in the states machine
	//	
	// The Component currently Hovered
	private int _currentArc;

	// The current state of the state machine
	private States _currentState;

	// The current location of the mouse.
	private Point _currentPoint;
	//
	// Variables used in the states machine
	//	

	// To check if the currentComponent have changed
	private Composite _oldComposite;

	// To check if the expert mode is activated
	private boolean _expertMode;

	public MarkedMenuIU(JMarkedMenu component) {
		_component = component;
		_arcs = new CustomArc2D[MAXCOMPONENTS];
		_outerCercle = new Ellipse2D.Double( 0,0, _component.getWidth(), _component.getHeight());

		for(int i=0; i < MAXCOMPONENTS; i++) {
			_arcs[i] = new CustomArc2D();
		}
		_currentArc = -1;
		_currentState = States.IDLE;
		_oldComposite = null;
		_expertMode = false;

		_component.addComponentListener(this);
		_component.addMouseMotionListener(this);
		_component.addMouseListener(this);
	} 

	public void setDefaultState() {
		_currentState = States.IDLE;
		_currentArc = -1;
	}


	@Override
	public void paint(Graphics g, JComponent c) {
		super.paint(g, c);

		if (_expertMode)
			return;

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.BLACK);
		g2.draw(_outerCercle);

		
		for(int i=0; i < nbElements;i++) {
			g2.setColor(_arcs[i]._color);
			g2.fill(_arcs[i]._arc);
			g2.setColor(Color.BLACK);
			g2.draw(_arcs[i]._arc);

			g2.translate(_component.getWidth()/2, _component.getHeight()/2);
			g2.rotate(-Math.toRadians(_arcs[i]._name._theta));
			g2.drawString(_arcs[i]._name._name, _arcs[i]._name._r,5);
			g2.rotate(Math.toRadians(_arcs[i]._name._theta));
			g2.translate(-_component.getWidth()/2, -_component.getHeight()/2);
			
		}		

	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseMoved(MouseEvent e) {
		_currentPoint = e.getPoint();

		switch (_currentState) {
		case IDLE:		
			if(_outerCercle.contains(e.getPoint())) {
				int i = 0;
				while(i < nbElements && !_arcs[i]._arc.contains(e.getPoint())) {
					i++;
				}

				_component.getModel().getCurrentComponent().getComponentByIndex(i).setArmed(true);
				_currentArc = i;

				_currentState = States.INSIDE;
			} else {
				_currentState = States.IDLE;
			}

			break;

		case INSIDE:
			if(_outerCercle.contains(e.getPoint())) {
				int i = 0;
				while(i < nbElements && !_arcs[i]._arc.contains(e.getPoint())) {
					i++;
				}

				if (i != _currentArc && i < MAXCOMPONENTS) {
					System.out.println(i);
					_component.getModel().getCurrentComponent().getComponentByIndex(i).setArmed(true);
					_component.getModel().getCurrentComponent().getComponentByIndex(_currentArc).setArmed(false);
					_currentArc = i;
				}

				_currentState = States.INSIDE;
			} else {
				DefaultMarkedMenuModel model = _component.getModel();
				Component comp = model.getCurrentComponent().getComponentByIndex(_currentArc);	
				comp.setArmed(false);	
				comp.setPressed(true);
				comp.setPressed(false);

				_currentState = States.IDLE;
			}
			break;
		}	

	}


	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		_outerCercle.setFrame( 0,0, _component.getWidth(), _component.getHeight());

		for(int i=0; i < nbElements;i++) {
			_arcs[i]._arc.setFrame( 0,0, _component.getWidth(), _component.getHeight());
		}

		_component.repaint();

	}

	@Override
	public void componentShown(ComponentEvent l) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stateChanged(ChangeEvent l) {
		DefaultMarkedMenuModel model = (DefaultMarkedMenuModel)l.getSource();

		// TODO : check the mode
		_expertMode = model.getExpertMode();

		// TODO : Model give object and update UI		
		Composite currentComposite = model.getCurrentComponent();
		Component[] components = model.getCurrentComponent().getChilds();
		nbElements = Math.min(MAXCOMPONENTS, model.getCurrentComponent().getChildsCount());

		//TODO Not Here: 
		if(_oldComposite != null && currentComposite != _oldComposite) {
			Point p = new Point(_currentPoint.x + _component.getLocation().x , _currentPoint.y + _component.getLocation().y );
			_component.setCenter(p);
		}
		_oldComposite = currentComposite;

		float x = 0;
		float y = 0;
		float w = _component.getWidth();
		float h = _component.getHeight();
		int type = Arc2D.PIE;
		float extent = FULL_ROTATION / nbElements;

		int textRadius = (int) ((_component.getWidth()/2)*0.3);
		float textAngle = extent/2;
				
		for(int i=0; i < nbElements;i++) {
			CustomArc2D currentArc = _arcs[i];
			currentArc._arc.setArc(x, y, w, h, extent*i, extent, type);
			currentArc._color = components[i].isArmed() ? ARMED : REST;
			currentArc._name._name = components[i].toString();

			currentArc._name._r = textRadius;
			currentArc._name._theta = (int) textAngle;
			
			textAngle+= extent;
		}

		_component.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

		if (_currentState == States.INSIDE) {
			DefaultMarkedMenuModel model = _component.getModel();
			Component comp = model.getCurrentComponent().getComponentByIndex(_currentArc);	
			comp.setArmed(false);	
			comp.setPressed(true);
			comp.setPressed(false);

			_currentState = States.IDLE;
		}
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	private class CustomArc2D {

		protected Arc2D.Float _arc;

		protected CustomString _name;

		protected Color _color;

		public CustomArc2D() {
			super();
			_name = new CustomString();
			_color = REST;
			_arc = new Arc2D.Float();
		}

		public CustomString getName() {
			return _name;
		}

		public Color getColor() {
			return _color;
		}

		public Arc2D.Float getArc() {
			return _arc;
		}

		public void setColor(Color c) {
			_color = c;
		}

		public void setName(CustomString name) {
			_name = name;
		}
		
		
		private class CustomString{
		
			private String _name;
			
			private int _r;
			
			private int _theta;
			
			public CustomString() {
				_name = new String();
				_r = 0;
				_theta = 0;
			}
			
			// LA FONTE SA MERE
			
		}


	}
}
