package paint;

//////////////////////////////////////////////////////////////////////////////

//file    : Paint.java
//content : basic painting app
//////////////////////////////////////////////////////////////////////////////

/* imports *****************************************************************/

import static java.lang.Math.*;

import java.util.HashMap;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Point;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D.Double;
import java.awt.event.*;
import javax.swing.event.*;

import markedMenu.component.JMarkedMenu;
import markedMenu.model.composite.Component;
import markedMenu.model.composite.Composite;
import markedMenu.model.composite.Leaf;
import markedMenu.uiDelegate.MarkedMenuIU;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.AbstractAction;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

/* paint *******************************************************************/

class Paint extends JFrame {
	Vector<Shape> shapes = new Vector<Shape>();
	Vector<Color> colors = new Vector<Color>();
	static Color currentColor = Color.BLACK;
	static Shape currentShape = null;
	JMarkedMenu markedMenu;
	public boolean displayed = false;

	class Tool extends AbstractAction implements MouseInputListener {
		Point o;
		Shape shape;
		JColorChooser colorChooser;

		public Tool(String name) {
			super(name);
			colorChooser = new JColorChooser();
		}

		public void actionPerformed(ActionEvent e) {
			System.out.println("using tool " + this);
			panel.removeMouseListener(tool);
			panel.removeMouseMotionListener(tool);
			tool = this;
			panel.addMouseListener(tool);
			panel.addMouseMotionListener(tool);
		}

		public void mouseClicked(MouseEvent e) {
			System.out.println("click");
			if(e.getButton() == MouseEvent.BUTTON3) {
				markedMenu.activated(e.getPoint());
			}
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
			o = e.getPoint();
		}

		public void mouseReleased(MouseEvent e) {
			shape = null;
		}

		public void mouseDragged(MouseEvent e) {
		}

		public void mouseMoved(MouseEvent e) {
		}
	}

	Tool tools[] = { new Tool("pen") {
		public void mouseDragged(MouseEvent e) {
			Path2D.Double path = (Path2D.Double) shape;
			if (path == null) {
				path = new Path2D.Double();
				path.moveTo(o.getX(), o.getY());
				shapes.add(shape = path);
				colors.add(currentColor);
			}
			path.lineTo(e.getX(), e.getY());
			panel.repaint();
		}
	}, new Tool("rect") {
		public void mouseDragged(MouseEvent e) {
			Rectangle2D.Double rect = (Rectangle2D.Double) shape;
			if (rect == null) {
				rect = new Rectangle2D.Double(o.getX(), o.getY(), 0, 0);
				shapes.add(shape = rect);
				colors.add(currentColor);
			}
			rect.setRect(min(e.getX(), o.getX()), min(e.getY(), o.getY()), abs(e.getX() - o.getX()),
					abs(e.getY() - o.getY()));
			panel.repaint();
		}
	}, new Tool("ellipse") {
		public void mouseDragged(MouseEvent e) {
			Ellipse2D.Double ellipse = (Ellipse2D.Double) shape;
			if (ellipse == null) {
				ellipse = new Ellipse2D.Double(o.getX(), o.getY(), 0, 0);
				shapes.add(shape = ellipse);
				colors.add(currentColor);
			}
			ellipse.setFrame(min(e.getX(), o.getX()), min(e.getY(), o.getY()), abs(e.getX() - o.getX()),
					abs(e.getY() - o.getY()));

			panel.repaint();
		}
	}, new Tool("color") {
		public void actionPerformed(ActionEvent e) {
			colorChooser.setColor(currentColor);
			JDialog dialog = JColorChooser.createDialog(
					null, 
					"Pick a Color", 
					true, 
					colorChooser, 
					new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							currentColor = colorChooser.getColor();
						}

					},  
					null); 
			dialog.setVisible(true);
		}
	}, new Tool("ExpertMode") {
		public void actionPerformed(ActionEvent e) {
			markedMenu.setExpertMode(!markedMenu.getExpertMode());			
		}
		
	}
	};

	Tool tool;

	JPanel panel;

	public Paint(String title) {
		super(title);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(800, 600));
		markedMenu = createMenu();

		add(new JToolBar() {
			{
				for (AbstractAction tool : tools) {
					add(tool);
				}
			}
		}, BorderLayout.NORTH);


		add(panel = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D) g;
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				g2.setColor(Color.WHITE);
				g2.fillRect(0, 0, getWidth(), getHeight());

				for(int i=0; i < shapes.size(); i++) {
					g2.setColor(colors.get(i));
					g2.draw(shapes.get(i));					
				}
			}
		});
		panel.add(markedMenu);
		panel.setLayout(null);

		pack();
		setVisible(true);
	}

	private JMarkedMenu createMenu() {
		JMarkedMenu markedMenu = new JMarkedMenu();
		// Create component of markedmenu
		Component ellipse = new Leaf("Ellipse");
		Component rectangle = new Leaf("Rectangle");
		Component pen = new Leaf("Pen");
		Composite shape = new Composite("Shape");
		shape.addComponent(rectangle);
		shape.addComponent(pen);
		shape.addComponent(ellipse);
		Component blue = new Leaf("Blue");
		Component red = new Leaf("Red");
		Component green = new Leaf("Green");
		Component yellow = new Leaf("Yellow");
		Component dark = new Leaf("Dark");
		Component white = new Leaf("White");
		Component purple = new Leaf("Purple");
		Component pink = new Leaf("Pink");
		Component brown = new Leaf("Brown");
		Component grey = new Leaf("Grey");
		Composite color = new Composite("Color");
		color.addComponent(blue);
		color.addComponent(red);
		color.addComponent(green);
		color.addComponent(yellow);
		color.addComponent(dark);
		color.addComponent(white);
		color.addComponent(purple);
		color.addComponent(pink);
		color.addComponent(brown);
		color.addComponent(grey);
		markedMenu.addComponent(color);
		markedMenu.addComponent(shape);
		markedMenu.setSize(markedMenu.getPreferredSize());
		markedMenu.setExpertMode(false);

		markedMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Component c = (Component) e.getSource();

				if (c == blue) {
					currentColor = Color.BLUE;
				}else if(c == red) {
					currentColor = Color.RED;
				}else if(c == green) {
					currentColor = Color.GREEN;
				}else if(c == yellow) {
					currentColor = Color.YELLOW;
				}else if(c == dark) {
					currentColor = Color.BLACK;
				}else if(c == white) {
					currentColor = Color.WHITE;
				}else if(c == purple) {
					currentColor = new Color(204,0,255);
				}else if(c == pink) {
					currentColor = Color.PINK;
				}else if(c == brown) {
					currentColor = new Color(204,100,0);
				}else if(c == grey) {
					currentColor = Color.GRAY;
				}else if(c == ellipse) {
					panel.removeMouseListener(tool);
					panel.removeMouseMotionListener(tool);
					tool = tools[2];
					panel.addMouseListener(tool);
					panel.addMouseMotionListener(tool);
				}else if(c == rectangle) {
					panel.removeMouseListener(tool);
					panel.removeMouseMotionListener(tool);
					tool = tools[1];
					panel.addMouseListener(tool);
					panel.addMouseMotionListener(tool);
				}else if(c == pen) {
					panel.removeMouseListener(tool);
					panel.removeMouseMotionListener(tool);
					tool = tools[0];
					panel.addMouseListener(tool);
					panel.addMouseMotionListener(tool);
				}
			}
		});

		return markedMenu;
	}

	/* main *********************************************************************/

	public static void main(String argv[]) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Paint paint = new Paint("paint");
			}
		});
	}
}