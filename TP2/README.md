# TP2 IHM Avancée - INFO5 Polytech

Etudiants:
- Arnoux Thibaut  
- Lordey Maxime  
- Rakotoarimalala Mandresy  

## Développement du MarkedMenu

### Conception

Pour réaliser ce markedMenu, nous avons implementé notre propre composant swing. Pour cela, nous avons utilisé un patern composite. Le composant swing est donc constitué de noeuds et de feuilles, où chaque noeud peut contenir d'autres noeuds et feuilles.  
Les noeuds et les feuilles sont ajoutés de manière dynamique au composant.


On peut visualiser le diagramme des classes:

![](./docs/MarkedMenu.png)


### Utilisation

Pour utiliser ce markedMenu, il faut effectuer un clic droit pour l'afficher, le markedMenu apparait centré sur la souris de l'utilisateur.  
Si l'on souhaite ne plus l'afficher, on effectue un second clic droit.  
L'utilisation ce fait uniquement avec le déplacement de la souris. Si le déplacement de la souris sort du markedMenu, alors on déclenche le menu sur lequel on était.  
Si l'utilisateur était sur un noeud, alors on affiche les sous-menus correspondant.  
Si il s'agit d'une feuille, alors on déclenche son effet, par exemple un changement de couleur, ou de forme.  
Si le menu contient plus 8 composants (noeud ou feuille), un composant "Other" apparait pour avoir accès au reste.

### Visuel

Premier menu lors du clic droit, où l'utilisateur est sur "Color":  
![](./docs/StartMenu.png)  

Menu Couleur apres déclenchement du composant "Color":  
![](./docs/ColorMenu.png)  
Comme on peut le voir ce menu contient plus de 8 composants, il y a donc "Other" qui apparait.  


Menu contenant le reste des couleurs après déclenchement du composant "Other":  
![](./docs/OtherColorMenu.png)  
On peut ensuite choisir l'une de ces couleurs qui changera le comportement du paint.
